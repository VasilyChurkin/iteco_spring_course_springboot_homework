package ru.churkin.hometeco.springcourse_boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcourseBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcourseBootApplication.class, args);
    }

}
