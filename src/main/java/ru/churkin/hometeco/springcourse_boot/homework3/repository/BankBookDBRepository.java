package ru.churkin.hometeco.springcourse_boot.homework3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.BankBookEntity;

public interface BankBookDBRepository extends JpaRepository<BankBookEntity, Integer> {
}
