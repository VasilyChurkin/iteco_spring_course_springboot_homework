package ru.churkin.hometeco.springcourse_boot.homework3.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.ErrorDto;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ValidationExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorDto handleConstraintViolationException(ConstraintViolationException exception) {
        return ErrorDto.builder().status(HttpStatus.BAD_REQUEST).message(exception.getMessage()).build();
    }
}
