package ru.churkin.hometeco.springcourse_boot.homework3.repository;

import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.BankBookDto;

import java.util.List;

public interface BankBookRepository {

    BankBookDto getBankBookById(Integer id);

    List<BankBookDto> getBankBookByUserId(Integer userId);

    BankBookDto addBankBook(BankBookDto bankBookDto);

    Boolean checkUnique(BankBookDto bankBookDto);

    BankBookDto updateBankBook(BankBookDto bankBookDto);

    Boolean removeBankBookById(Integer bankBookDto);
}
