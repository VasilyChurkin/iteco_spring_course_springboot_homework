package ru.churkin.hometeco.springcourse_boot.homework3.service;

import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.BankBookDto;

import java.util.List;

public interface BankBookService {

    List<BankBookDto> getBankBookByUserId(Integer userId);

    BankBookDto getBankBookById(Integer id);

    BankBookDto createBankBook(BankBookDto bankBook);

    BankBookDto updateBankBook(BankBookDto bankBook);

    Boolean removeBankBookById(Integer bankBook);

    Boolean removeAllBankBookForUser(Integer userId);

}
