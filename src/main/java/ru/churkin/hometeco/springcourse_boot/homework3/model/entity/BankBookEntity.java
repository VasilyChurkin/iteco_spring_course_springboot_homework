package ru.churkin.hometeco.springcourse_boot.homework3.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Data
@Table(schema = "bank", name = "bankbook")
public class BankBookEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "userid")
    Integer userId;

    @Column(name = "bankbooknumber")
    Integer number;

    @Column(name = "amount")
    BigDecimal amount;

    //    @Column(name = "currency")
//    @OneToOne(cascade = CascadeType.ALL)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency", referencedColumnName = "id")
    CurrencyEntity currency;
}
