package ru.churkin.hometeco.springcourse_boot.homework3.model.dto;

import lombok.Data;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.CreateDtoMarker;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.Currency;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.UpdateDtoMarker;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class CurrencyDto {

    @NotNull(groups = UpdateDtoMarker.class)
    @Null(groups = CreateDtoMarker.class)
    Integer id;

    @Currency
    String name;
}
