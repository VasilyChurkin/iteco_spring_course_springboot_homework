package ru.churkin.hometeco.springcourse_boot.homework3.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class HealthCheckController {

    @GetMapping("/healthCheck")
    public String getTest(){
        return "healthCheck status: OK. Time: " + LocalDateTime.now();
    }
}
