package ru.churkin.hometeco.springcourse_boot.homework3.exception;

public class BankBookNotFoundException extends RuntimeException {
    public BankBookNotFoundException(String s) {
        super(s);
    }
}
