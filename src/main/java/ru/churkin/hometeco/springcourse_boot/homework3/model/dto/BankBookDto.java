package ru.churkin.hometeco.springcourse_boot.homework3.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.CreateDtoMarker;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.Currency;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.UpdateDtoMarker;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankBookDto {

    @NotNull(groups = UpdateDtoMarker.class, message = "Required non null")
    @Null(groups = CreateDtoMarker.class, message = "Require null")
    Integer id;

    Integer userId;

    @NotBlank
    String number;

    @PositiveOrZero
    BigDecimal amount;

    @Currency
    String currency;
}
