package ru.churkin.hometeco.springcourse_boot.homework3.validation;

import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.CurrencyEntity;
import ru.churkin.hometeco.springcourse_boot.homework3.repository.CurrencyRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CurrencyValidator implements ConstraintValidator<Currency, String> {

    public CurrencyValidator(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    private final CurrencyRepository currencyRepository;

    @Override
    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        return getAllCurrencies().stream()
                .map(CurrencyEntity::getName)
                .anyMatch(name::equals);
    }

    private List<CurrencyEntity> getAllCurrencies(){
        Optional<List<CurrencyEntity>> opt = Optional.of(currencyRepository.findAll());
        return opt.orElse(new ArrayList<>());
    }


}
