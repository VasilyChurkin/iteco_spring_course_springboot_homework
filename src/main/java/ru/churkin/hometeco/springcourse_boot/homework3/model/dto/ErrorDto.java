package ru.churkin.hometeco.springcourse_boot.homework3.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@Builder
public class ErrorDto {

    HttpStatus status;
    String message;
}
