package ru.churkin.hometeco.springcourse_boot.homework3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.CurrencyEntity;

@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Integer> {

}
