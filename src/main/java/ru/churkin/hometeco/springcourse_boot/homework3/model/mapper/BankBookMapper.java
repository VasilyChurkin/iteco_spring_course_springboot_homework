package ru.churkin.hometeco.springcourse_boot.homework3.model.mapper;

import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.BankBookDto;
import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.CurrencyDto;
import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.BankBookEntity;
import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.CurrencyEntity;
import ru.churkin.hometeco.springcourse_boot.homework3.repository.CurrencyRepository;

@Component
public  class BankBookMapper {

    public BankBookMapper(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    private final CurrencyRepository currencyRepository;

    public BankBookEntity mapDtoToEntity(BankBookDto dto) {
        BankBookEntity entity = new BankBookEntity();
        entity.setId(dto.getId());
        entity.setAmount(dto.getAmount());
        entity.setCurrency(currencyRepository.findAll().stream()
                .filter(e->dto.getCurrency().equals(e.getName()))
                .findFirst().get());
        entity.setNumber(Integer.valueOf(dto.getNumber()));
        entity.setUserId(dto.getUserId());
        return entity;
    }

    public static BankBookDto mapEntityToDto(BankBookEntity entity) {
        BankBookDto dto = new BankBookDto();
        dto.setId(entity.getId());
        dto.setAmount(entity.getAmount());
        dto.setCurrency(entity.getCurrency().getName());
        dto.setNumber(entity.getNumber().toString());
        dto.setUserId(entity.getUserId());
        return dto;
    }

    public static CurrencyDto maptoDto(CurrencyEntity entity) {
        CurrencyDto dto = new CurrencyDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

    public static CurrencyEntity mapToEntity(CurrencyDto dto) {
        CurrencyEntity entity = new CurrencyEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }
}
