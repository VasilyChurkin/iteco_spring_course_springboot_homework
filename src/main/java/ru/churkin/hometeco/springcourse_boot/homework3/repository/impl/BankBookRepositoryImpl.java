package ru.churkin.hometeco.springcourse_boot.homework3.repository.impl;

import org.springframework.stereotype.Controller;
import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.BankBookDto;
import ru.churkin.hometeco.springcourse_boot.homework3.repository.BankBookRepository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Controller
public class BankBookRepositoryImpl implements BankBookRepository {

    private final Map<Integer, BankBookDto> storage = new ConcurrentHashMap<>();
    private final AtomicInteger sequenceId = new AtomicInteger(1);

    @PostConstruct
    private void init() {
//        Integer id = sequenceId.getAndIncrement();
//        BankBookDto bankBook = new BankBookDto(id, 1, "123", BigDecimal.valueOf(1000), "USD");
//        storage.put(id, bankBook);
    }

    @Override
    public BankBookDto getBankBookById(Integer id) {
        return storage.get(id);
    }

    @Override
    public List<BankBookDto> getBankBookByUserId(Integer userId) {
        return storage.values().stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public BankBookDto addBankBook(BankBookDto bankBookDto) {
        Integer id = sequenceId.getAndIncrement();
        bankBookDto.setId(id);
        storage.put(bankBookDto.getId(), bankBookDto);
        return bankBookDto;
    }

    @Override
    public BankBookDto updateBankBook(BankBookDto bankBookDto) {
        BankBookDto existBankBook = storage.get(bankBookDto.getId());
        if (existBankBook == null) {
            throw new IllegalArgumentException("Cannot update not-existing bankBook");
        }
        if (!existBankBook.getNumber().equals(bankBookDto.getNumber())) {
            throw new IllegalArgumentException("Cannot change of bankbook number");
        }
        storage.put(bankBookDto.getId(), bankBookDto);
        return bankBookDto;
    }

    @Override
    public Boolean removeBankBookById(Integer bankBookId) {
        Boolean res = false;
        if (!Objects.isNull(storage.remove(bankBookId))) {
            res = true;
        }
        return res;
    }

    @Override
    public Boolean checkUnique(BankBookDto bankBookDto) {
        List<BankBookDto> existBankBook = storage.values().stream()
                .filter(e -> e.getUserId().equals(bankBookDto.getUserId()))
                .filter(e -> e.getNumber().equals(bankBookDto.getNumber()))
                .filter(e -> e.getCurrency().equals(bankBookDto.getCurrency()))
                .collect(Collectors.toList());
        return existBankBook.isEmpty();
    }
}
