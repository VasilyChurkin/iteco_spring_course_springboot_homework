package ru.churkin.hometeco.springcourse_boot.homework3.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.BankBookDto;
import ru.churkin.hometeco.springcourse_boot.homework3.service.BankBookService;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.CreateDtoMarker;
import ru.churkin.hometeco.springcourse_boot.homework3.validation.UpdateDtoMarker;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/bank-book")
@RequiredArgsConstructor
@Validated
public class BankBookController {

    private final BankBookService bankBookService;

    /**
     * 1)
     * Возвращает список BankBookDto пользователя по BankBookDto.userId GET /bank-book/by-user-id/{userId}
     * (userId - обязательный, если нет, то возвращать ErrorDto с информацией)
     * GET http://localhost:8080/bank-book/by-user-id?userId=1
     */
    @GetMapping("/by-user-id")
    List<BankBookDto> getBankBookByUserId(@RequestParam(required = false) Integer userId){
        return bankBookService.getBankBookByUserId(userId);
    }

    /**
     * 2)
     * Возвращает BankBookDto по BankBookDto.id GET  /bank-book/{bankBookId} (bankBookId - обязательный,
     * если нет, то возвращать ErrorDto с информацией; если в нашем хранилище нет объекта с bankBookId,
     * то генерируем исключение и обрабатываем его с результатом ErrorDto.NOT_FOUND)
     * @RequestParam - на основании сообщения в чате
     * GET http://localhost:8080/bank-book?bankBookId=1
     */
    @GetMapping("")
    BankBookDto getBankBookById(@RequestParam(required = false) Integer bankBookId){
        return bankBookService.getBankBookById(bankBookId);
    }

    /**
     * 3)
     * Возвращает созданный лицевой счет POST /bank-book BODY: BankBookDto (если у BankBookDto.userId уже
     * есть лицевой счет и номера счетов (number) совпадают, то проверяем тип валюты (currency), если они разные,
     * то добавляем счет, если одинаковые, то генерируем исключение - счет с данной валютой уже имеется в хранилище:
     * BankBookDto.id и обрабатываем исключение перед ответом)
     * POST http://localhost:8080/bank-book
     * Example body:
     * {
     *     "userId": 12,
     *     "number": "123",
     *     "amount": 100000,
     *     "currency": "USD"
     * }
     */
    @PostMapping("")
    @Validated(CreateDtoMarker.class)
    BankBookDto createBankBook(@RequestBody @Valid BankBookDto bankBook){
        return bankBookService.createBankBook(bankBook);
    }

    /**
     * 4)
     * Возвращает обновленный лицевой счет PUT /bank-book BODY: BankBookDto (по запросу обновляем счет в хранилище,
     * но обновлять можем только userId, amount и currency, если пытаемся изменить number, то генерируем исключение
     *  - номер менять нельзя и обрабатываем исключение перед ответом)
     *  PUT http://localhost:8080/bank-book
     */
    @PutMapping("")
    @Validated(UpdateDtoMarker.class)
    BankBookDto updateBankBook(@RequestBody @Valid BankBookDto bankBook){
        return bankBookService.updateBankBook(bankBook);
    }

    /**
     * 5)
     * Возвращает результат удаления счета (HTTP код) DELETE /bank-book/{bankBookId}
     * Успешно удалено - 200
     * не удалено - 400
     * DELETE http://localhost:8080/bank-book/1
     */
    @DeleteMapping("/{bankBookId}")
    Integer removeBankBookById(@PathVariable(required = true) Integer bankBookId){
        if (bankBookService.removeBankBookById(bankBookId)) {
            return HttpStatus.OK.value();
        }
        return HttpStatus.BAD_REQUEST.value();
    }

    /**
     * 6)
     * Возвращает результат удаления счетов пользователя (HTTP код) DELETE /bank-book/by-user-id/{userId}
     * (Удаляет все счета пользователя из хранилища по BankBookDto.userId)
     * DELETE http://localhost:8080/bank-book/by-user-id/1
     */
    @DeleteMapping("/by-user-id/{userId}")
    Integer removeAllBankBookForUser(@PathVariable Integer userId){
        if (bankBookService.removeAllBankBookForUser(userId)) {
            return HttpStatus.OK.value();
        }
        return HttpStatus.BAD_REQUEST.value();
    }
}
