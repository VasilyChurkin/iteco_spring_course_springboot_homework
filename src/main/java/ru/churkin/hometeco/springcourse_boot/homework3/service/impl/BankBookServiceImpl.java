package ru.churkin.hometeco.springcourse_boot.homework3.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse_boot.homework3.exception.BankBookNotFoundException;
import ru.churkin.hometeco.springcourse_boot.homework3.model.dto.BankBookDto;
import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.BankBookEntity;
import ru.churkin.hometeco.springcourse_boot.homework3.model.entity.CurrencyEntity;
import ru.churkin.hometeco.springcourse_boot.homework3.model.mapper.BankBookMapper;
import ru.churkin.hometeco.springcourse_boot.homework3.repository.BankBookDBRepository;
import ru.churkin.hometeco.springcourse_boot.homework3.repository.CurrencyRepository;
import ru.churkin.hometeco.springcourse_boot.homework3.service.BankBookService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.churkin.hometeco.springcourse_boot.homework3.model.mapper.BankBookMapper.mapEntityToDto;


@Component
@RequiredArgsConstructor
public class BankBookServiceImpl implements BankBookService {

    private final BankBookDBRepository bankBookDBRepository;
    private final CurrencyRepository currencyRepository;
    private final BankBookMapper mapper;

    @Override
    public List<BankBookDto> getBankBookByUserId(Integer userId) {
        Optional<List<BankBookDto>> bbe = Optional.of(bankBookDBRepository.findAll().stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(BankBookMapper::mapEntityToDto)
                .collect(Collectors.toList()));
        return bbe.orElseThrow(() -> new BankBookNotFoundException(String.format("BankBook with userid = %d not found", userId)));
    }

    @Override
    public BankBookDto getBankBookById(Integer id) {
        Optional<BankBookDto> res = Optional.of(mapEntityToDto(bankBookDBRepository.getById(id)));
        return res.orElseThrow(() ->new BankBookNotFoundException(String.format("BankBook with id = %d not found", id)));
    }

    @Override
    public BankBookDto createBankBook(BankBookDto bankBook) {
        BankBookEntity entity = mapper.mapDtoToEntity(bankBook);
        return mapEntityToDto(bankBookDBRepository.save(entity));
    }

    @Override
    public BankBookDto updateBankBook(BankBookDto bankBook) {
        BankBookEntity entity = bankBookDBRepository.findById(bankBook.getId())
                .orElseThrow(() -> new IllegalArgumentException("no Bankbook with such Id"));

        entity.setUserId(mapper.mapDtoToEntity(bankBook).getId());
        entity.setNumber(mapper.mapDtoToEntity(bankBook).getNumber());
        entity.setAmount(mapper.mapDtoToEntity(bankBook).getAmount());

        CurrencyEntity currencyEntity = currencyRepository.findAll()
                .stream()
                .filter(e-> e.getName().equals(bankBook.getCurrency()))
                .findFirst().get();
        entity.setCurrency(currencyEntity);

        return mapEntityToDto(bankBookDBRepository.save(entity));
    }

    @Override
    public Boolean removeBankBookById(Integer bankBookId) {
        bankBookDBRepository.findById(bankBookId)
                .orElseThrow(() -> new IllegalArgumentException("no such bankBook id for remove"));
        bankBookDBRepository.deleteById(bankBookId);
        return true;
    }

    @Override
    public Boolean removeAllBankBookForUser(Integer userId) {
        List<BankBookDto> dtos = Arrays.asList(mapEntityToDto(bankBookDBRepository.getById(userId)));

        return true;
    }
}
